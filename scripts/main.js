let boxes = [];
const playerSupport = document.querySelector(".player-support");
const resetButton=document.querySelector(".reset-button");
let gameBoard = [[], [], []];
let currentPlayer = "X";
for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
        gameBoard[i][j] = " ";
    }
}
function clearBoard() {
    for (let i = 0; i < 9; i++) {
        boxes[i].textContent = " ";
    }
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            gameBoard[i][j] = " ";
        }
    }
    currentPlayer = "X";
    playerSupport.textContent = `This is Player ${currentPlayer} turn`;
}
function evaluateGame() {
    let evaluationstate = "not-finished";
    for (let i = 0; i < 3; i++) {
        if ((gameBoard[i][0] === "X" || gameBoard[i][0] === "O") && gameBoard[i][0] === gameBoard[i][1] && gameBoard[i][1] === gameBoard[i][2]) {
            alert(`Player ${gameBoard[i][0]} won the match`);
            evaluationstate = "finished"
        }
    }
    if (evaluationstate != "finished") {
        for (let j = 0; j < 3; j++) {
            if ((gameBoard[0][j] === "X" || gameBoard[0][j] === "O") && gameBoard[0][j] === gameBoard[1][j] && gameBoard[1][j] === gameBoard[2][j]) {
                alert(`Player ${gameBoard[0][j]} won the match`);
                evaluationstate = "finished"
            }
        }
    }
    if (evaluationstate != "finished") {
        if ((gameBoard[1][1] === "X" || gameBoard[1][1] === "O") && gameBoard[0][0] === gameBoard[1][1] && gameBoard[1][1] === gameBoard[2][2]) {
            alert(`Player ${gameBoard[1][1]} won the match`);
            evaluationstate = "finished"
        }
    }
    if (evaluationstate != "finished") {
        if ((gameBoard[1][1] === "X" || gameBoard[1][1] === "O") && gameBoard[0][2] === gameBoard[1][1] && gameBoard[1][1] === gameBoard[2][0]) {
            alert(`Player ${gameBoard[1][1]} won the match`);
            evaluationstate = "finished"
        }
    }
    if (evaluationstate === "finished") {
        clearBoard();
    }
    else {
        let i = 0;
        let j = 0;
        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                if (gameBoard[i][j] != "X" && gameBoard[i][j] != "O") {
                    i = 10;
                    break;
                }
            }
        }
        if (i == 3) {
            alert("Match is tie");
            clearBoard();
        }
    }

}

function addToBoard(rowPos, colPos, index) {
    boxes[index].textContent = currentPlayer;
    gameBoard[rowPos][colPos] = currentPlayer;
    if (currentPlayer == "X") {

        currentPlayer = "O";
    }
    else {
        currentPlayer = "X";
    }
    if (playerSupport) {
        playerSupport.textContent = `This is Player ${currentPlayer} turn`;
    }
    //give some time for settin
    let delayInMilliseconds = 500; //1 second

    setTimeout(function () {
        evaluateGame();
    }, delayInMilliseconds);
    
}
for (let i = 0; i < 9; i++) {
    boxes[i] = document.querySelector(`.box${i}`)
    boxes[i].addEventListener("click", () => {
        if(boxes[i].textContent==="X" || boxes[i].textContent=="O"){

        }
        else{
            switch (i) {
                case 0:
                    addToBoard(0, 0, i);
                    break;
                case 1:
                    addToBoard(0, 1, i);
                    break;
                case 2:
                    addToBoard(0, 2, i);
                    break;
                case 3:
                    addToBoard(1, 0, i);
                    break;
                case 4:
                    addToBoard(1, 1, i);
                    break;
                case 5:
                    addToBoard(1, 2, i);
                    break;
                case 6:
                    addToBoard(2, 0, i);
                    break;
                case 7:
                    addToBoard(2, 1, i);
                    break;
                case 8:
                    addToBoard(2, 2, i);
                    break;
            }
        }
    });
}
resetButton.addEventListener("click",()=>{
    clearBoard();
})